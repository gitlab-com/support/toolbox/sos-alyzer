#!/usr/bin/env bash

set -ue # set -o nounset errexit
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
set -o pipefail
IFS=$'\n\t'

# Only proceed if fast-stats is actually installed
if [ -z "$(command -v fast-stats)" ]; then
  echo 'fast-stats not installed... Aborting!'
  echo 'Please install it from https://gitlab.com/gitlab-com/support/toolbox/fast-stats/#installation '
  echo 'and add it to your PATH. Then, rerun this script.'
  exit 1
fi

LOGDIR="var/log/gitlab"
PROD="gitlab-rails/production_json.log"
GITALY="gitaly/current"
SHELL="gitlab-shell/gitlab-shell.log"
SIDEKIQ="sidekiq/current"
API="gitlab-rails/api_json.log"

# Define an array of regex patterns for common problem indicators
PROBLEM_PATTERNS=(
    '\bx509\b'
    'exiftool command failed'
    'status\":[4-5][0-9][0-9]'
    '\ [4-5][0-9][0-9]\ [0-9]'
    '\b(backtrace|cannot|Could not|deprecat\w+|eof|err(or)?|denied|fail(ed)?|fata(l)?|invalid|kill(ed)?|panic|Rack_Attack|rake aborted|time.?out|warn(ing)?)\b'
    'logrus Level'
    'exclusive lease'
)

# Join the patterns with '|' to create a single regex
COMBINED_PATTERN=$(IFS='|'; echo "${PROBLEM_PATTERNS[*]}")

## grep all logs for common problem indicators
grep -Eirn "$COMBINED_PATTERN" "$LOGDIR" | tee -a error.log

## more-than-single-digit CPU impact
grep -i 'git' ps | grep -P '(\s+\d+)\d+\.' | tee -a cpu-hogs.txt

## fast-stats production_json.log
if [ -f "$LOGDIR/$PROD" ]; then
  printf "\n\n### $PROD\n\n" >> stats.txt
  fast-stats errors "$LOGDIR/$PROD" | tee -a stats.txt
  fast-stats "$LOGDIR/$PROD" | tee -a stats.txt
  fast-stats top "$LOGDIR/$PROD" | tee -a stats.txt
fi

## fast-stats gitaly/current
if [ -f "$LOGDIR/$GITALY" ]; then
  printf "\n\n### $GITALY\n\n" >> stats.txt
  fast-stats errors "$LOGDIR/$GITALY" | tee -a stats.txt
  fast-stats "$LOGDIR/$GITALY" | tee -a stats.txt
  fast-stats top "$LOGDIR/$GITALY" | tee -a stats.txt
fi

## fast-stats gitlab-shell.log
if [ -f "$LOGDIR/$SHELL" ]; then
  printf "\n\n### $SHELL\n\n" >> stats.txt
  fast-stats errors "$LOGDIR/$SHELL" | tee -a stats.txt
  fast-stats "$LOGDIR/$SHELL" | tee -a stats.txt
  fast-stats top "$LOGDIR/$SHELL" | tee -a stats.txt
fi

## fast-stats sidekiq/current
if [ -f "$LOGDIR/$SIDEKIQ" ]; then
  printf "\n\n### $SIDEKIQ\n\n" >> stats.txt
  fast-stats errors "$LOGDIR/$SIDEKIQ" | tee -a stats.txt
  fast-stats "$LOGDIR/$SIDEKIQ" | tee -a stats.txt
  fast-stats top "$LOGDIR/$SIDEKIQ" | tee -a stats.txt
fi

## fast-stats api_json.log
if [ -f "$LOGDIR/$API" ]; then
  printf "\n\n### $API\n\n" >> stats.txt
  fast-stats errors "$LOGDIR/$API" | tee -a stats.txt
  fast-stats "$LOGDIR/$API" | tee -a stats.txt
  fast-stats top "$LOGDIR/$API" | tee -a stats.txt
fi
