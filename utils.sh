### single sos summary

# number of errors in error.log
wc -l error.log
# logs with most errors
awk -F ':' '{print $1}' gitlabsos.mayescprda011_20220125101057/error.log | sort | uniq -c | sort -nr

### multi-directory summary

# number of errors in error.logs
find . -name error.log -exec wc -l {} \; | sort -nr
# logs with most errors (aggregate)
awk -F ':' '{print $1}' **/error.log | sort | uniq -c | sort -nr
# logs with most errors (individual)
