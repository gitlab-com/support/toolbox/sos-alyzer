#!/usr/bin/env bash
set -u
set -e
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
set -o pipefail
IFS=$'\n\t'
SOSDIRS="$(ls -d gitlabsos*/)"

for dir in $SOSDIRS; do
	cd "$(pwd)/$dir"
    curl -L https://gitlab.com/gitlab-com/support/toolbox/sos-alyzer/-/raw/main/sos-alyzer.sh | bash
    cd ..
done
